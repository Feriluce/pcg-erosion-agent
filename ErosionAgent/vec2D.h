#include <math.h>

#ifndef VEC2D_H
#define VEC2D_H

//Represents a 2D vector
struct Vec2D{
	double x;
	double y;

	Vec2D():x(0), y(0) {};
	Vec2D(double x1, double y1){
		x = x1;
		y = y1;
	};

	bool operator==(const Vec2D &rhs) const{
		return (x == rhs.x) && (y == rhs.y);
	}

	Vec2D operator+(const Vec2D &rhs) const{
		return Vec2D(x+rhs.x, y+rhs.y);
	}

	Vec2D operator-(const Vec2D &rhs) const{
		return Vec2D(x-rhs.x, y-rhs.y);
	}

	/**
	 *	Check to see if a position exists.
	 *	Not sure if this is the correct way to implement it.
	 */
	bool exists() {
		if (this)
			return true;
		else
			return false;
	}

	double length() {
		return sqrtf((float)x*(float)x + (float)y*(float)y);
	}

	double angle() {
		return atan2(y, x);
	}
};

#endif