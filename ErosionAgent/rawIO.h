#ifndef RAWLOADER_H
#define RAWLOADER_H

#include <string>

int* loadRaw(std::string filename);
void saveRaw(char* filename, int* data);
void saveRaw(char* filename, double* data);

#endif