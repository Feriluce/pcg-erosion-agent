#ifndef WATERAGENT_H
#define WATERAGENT_H

#include "terrainModel.h"
#include "constants.h"
#include <vector>

class WaterAgent{
public:
	void init(double* heightMap);

	void addSprings();
	void erode();
	void thermalErosion();

	WaterAgent();
	~WaterAgent();

private:
		terrainModel* model;
		double* avgWater;
		Season season;
		std::vector<int> springs;
		double* rainMap;

		double calculateFlow(int curIndex, int nextIndex, double flux);
		double calculateTransport(Vec2D velocityVector, int index);
		double interpolateSediment(double x, double y);
		double interpolateHeight(double x, double y);
		double getTiltAngle(Vec2D VelVector, int index);

		//Step 1-5
		void distributeWater();
		void simulateFlow();
		void erosionDeposition();
		void sedimentTransport();
		void evaporation();
		
		double processNeighbors(Vec2D& lowest, std::vector<Vec2D>& lowerNeighbors, double talus, int index);
		void checkNeighbor(Vec2D& lowest, std::vector<Vec2D>& lowerNeighbors, double talus, double& lowVal, double height,
			double &totalHeight, Vec2D pos);

		
};

#endif