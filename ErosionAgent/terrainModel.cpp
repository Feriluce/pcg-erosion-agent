#include "terrainModel.h"

#include "constants.h"

int getIndex(int x, int y){
	return y*MAPSIZE + x;
}

Vec2D getCoords(int i){
	int x = i % MAPSIZE;
	int y = i / MAPSIZE;

	return Vec2D(x, y);
}

double getAngle(Vec2D velocityVector) {
	return velocityVector.angle();
}

double getLength(Vec2D velocityVector) {
	return velocityVector.length();
}

double* createSlope(){
	double increment = (HEIGHTFACTOR-1)/(double)MAPSIZE;
	double* map = new double[MAPSIZE*MAPSIZE];
	double height = 0.0;


	for(int i = 0; i < MAPSIZE; i++){
		for(int j = 0; j < MAPSIZE; j++){
			map[getIndex(j, i)] = height;
		}
		height += increment;
	}

	return map;
}