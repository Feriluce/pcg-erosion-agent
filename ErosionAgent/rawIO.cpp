#include "rawIO.h"
#include <stdio.h>
#include <iostream>
#include "constants.h"

using namespace std;

int *loadRaw(string filename){
	unsigned char *data;
	int* heightMap;
	FILE *file = NULL;

	if((fopen_s(&file, filename.c_str(), "rb")) != 0)
		cout << "Could not open file" << endl;
	else
		cout << "File opened" << endl;

	data = new unsigned char[MAPSIZE*MAPSIZE*2];
	heightMap = new int[MAPSIZE*MAPSIZE];

	fread(data, MAPSIZE*MAPSIZE*2, 1, file);

	for (int i = 0; i < 100; i++)
		printf("%X ", data[i]);

	for(int i = 0; i < MAPSIZE*MAPSIZE*2; i += 2){
		heightMap[i/2] = (int)data[i+1] << 8 | data[i];
	}

	for (int i = 0; i < 100; i++)
		cout << heightMap[i] << "   ";


	//cin.get();
	delete []data;
	fclose(file);

	return heightMap;
}

void saveRaw(char* filename, int* data){
	unsigned char* binData;
	FILE *file = NULL;

	
	if((fopen_s(&file, filename, "wb")) != 0)
		cout << "Could not open file" << endl;
	else
		cout << "File opened" << endl;
	
	if(file == NULL)
		cout << "WHHHYYYY?";
	binData = new unsigned char[MAPSIZE*MAPSIZE*2];

		for(int i = 0; i < MAPSIZE*MAPSIZE*2; i += 2){
		unsigned char bigEnd = (char)((data[i/2] >> 8) & 0x000000ff);
		unsigned char littleEnd = (char)(data[i/2] & 0x000000ff);
		binData[i] = littleEnd;
		binData[i+1] = bigEnd;
	}

	for (int i = 0; i < 100; i++)
		printf("%X ", binData[i]);

	fwrite(binData, 1, MAPSIZE*MAPSIZE*2, file);

	delete []binData;
	fclose(file);
}

void saveRaw(char* filename, double* data){
	unsigned char* binData;

	FILE *file = NULL;
	int* roundData = new int[MAPSIZE*MAPSIZE];

	for(int i = 0; i < MAPSIZE*MAPSIZE; i++){
		int val = static_cast<int>(floor(data[i]/HEIGHTFACTOR*MAXHEIGHT + 0.5));

		if(val < 0)
			val = 0;
		if(val > MAXHEIGHT)
			val = MAXHEIGHT;
		roundData[i] = val;
	}
	
	if((fopen_s(&file, filename, "wb")) != 0)
		cout << "Could not open file" << endl;
	else
		cout << "File opened" << endl;
	
	if(file == NULL)
		cout << "WHHHYYYY?";
	binData = new unsigned char[MAPSIZE*MAPSIZE*2];

		for(int i = 0; i < MAPSIZE*MAPSIZE*2; i += 2){
		unsigned char bigEnd = (char)((roundData[i/2] >> 8) & 0x000000ff);
		unsigned char littleEnd = (char)(roundData[i/2] & 0x000000ff);
		binData[i] = littleEnd;
		binData[i+1] = bigEnd;
	}

	for (int i = 0; i < 100; i++)
		printf("%X ", binData[i]);

	fwrite(binData, 1, MAPSIZE*MAPSIZE*2, file);

	delete[] binData;
	delete[] roundData;
	fclose(file);
}