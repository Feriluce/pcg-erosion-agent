// ErosionAgent.cpp : Defines the entry point for the console application.
//

#include "rawIO.h"
#include <ctime>
#include <stdlib.h>
#include "waterAgent.h"
#include "constants.h"
#include <string>
#include <iostream>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	srand(time(0));
	
	//int* temp = loadRaw(path.append(filename));
	
	int* temp = loadRaw("dsquaremap.raw");
	
	double* heightMap = new double[MAPSIZE*MAPSIZE];
	
	//Convert to correct unites
	for(int i = 0; i < MAPSIZE*MAPSIZE; i++){
		double tmp = static_cast<double>(temp[i])/MAXHEIGHT;
		heightMap[i] = tmp * HEIGHTFACTOR;
	}

	delete[] temp;
	

	//double* heightMap = createSlope();

	WaterAgent agent;

	agent.init(heightMap);
	agent.addSprings();
	
	for(int i = 0; i < 1501; i++){
		//agent.erode();
		agent.thermalErosion();
		if(i % 50 == 0)
			std::cout << "Iterations: " << i << "\n";
	}

	saveRaw("result.raw", heightMap);

	delete[] heightMap;

	std::cin.get();
	return 0;
}