#ifndef CONSTANTS_H
#define CONSTANTS_H

const int MAPSIZE = 1025;
const int MAXHEIGHT = 65535;
const double DMAXHEIGHT = 65535.0;
const double HEIGHTFACTOR = 600.0;
const int WIDTHFACTOR = 2000;
const double RAINVARIATION = 0.1; //meter
const double SUMMERMAXRAIN = 1.0;
const double SUMMERMINRAIN = 0.5;
const double WINTERMAXRAIN = 2.0;
const double WINTERMINRAIN = 1.5;
const double MINSPRINGVOLUME = 50;
const double MAXSPRINGVOLUME = 100;
const double GRIDSPACING = 1.95; //(MAXHEIGHT/(double)HEIGHTFACTOR)*(double)WIDTHFACTOR;
const double PIPEAREA = GRIDSPACING * GRIDSPACING;
const double GRIDAREA = GRIDSPACING * GRIDSPACING;
const double GRAVITY = 9.82;
const double DELTATIME = 1.0;	//standard: 1.0
const double Kt = 1.0;
const double Ka = 1.0;
const double Ki = 1.0;
const double EROSIONAMOUNT = 0.01;
const double RAINSCALE = 0.1;

/* Neidhold:
 *	Kc	15
 *	Kd	0.5
 *	Ke	
 *	Ks	0.3
 */
const double SEDIMENTCAPACITY = 0.2;	//Kc 0.1
const double DEPOSITION = 0.5;			//Kd - deposition constant. 0.5
const double EVAPORATION = 0.09;		//Ke - evaporation constant. ~ mmHg at 10 degrees 0.092f
const double DISSOLVING = 0.3;			//Ks - dissolving constant, should probably be applied per rocktype. 0.3

const double GRANITEDISSOLVE = 0.4;
const double GRANITEDEPOSIT = 0.6;
const double SANDSTONEDISSOLVE = 0.6;
const double SANDSTONEDEPOSIT = 0.9;
const double LIMESTONEDISSOLVE = 0.8;
const double LIMESTONEDEPOSIT = 0.3;

//Perlin
const int OCTAVES = 4;
const double PERSISTENCE = 0.75;
const double SCALING = 50.0;//(DMAXHEIGHT/(double)2)/(double)OCTAVES;
const double DIAMONDSEED = 32768.0;
const int ROCKLAYERS = 15;

#endif