﻿#define _USE_MATH_DEFINES

#include "waterAgent.h"
#include "constants.h"
#include <algorithm>
#include <vector>
#include <iostream>
#include <math.h>
#include "perlinnoise.h"
#include <queue>

using namespace std;

WaterAgent::WaterAgent(){
	season = Season::SUMMER;
	model = new terrainModel();
	
	model->flux = new totalFlux[MAPSIZE*MAPSIZE];
	model->rockMap = new std::stack<rockType>[MAPSIZE*MAPSIZE];
	model->sedimentAmount = new double[MAPSIZE*MAPSIZE];
	model->velocityVector = new Vec2D[MAPSIZE*MAPSIZE];
	model->waterHeight = new double[MAPSIZE*MAPSIZE];
	avgWater = new double[MAPSIZE*MAPSIZE];
}

WaterAgent::~WaterAgent(){
	delete[] model->flux;
	delete[] model->rockMap;
	delete[] model->sedimentAmount;
	delete[] model->velocityVector;
	delete[] model->waterHeight;
	delete[] model;
	delete[] avgWater;
	delete[] rainMap;
}

void WaterAgent::init(double* heightMap){
	model->heightMap = heightMap;


	for(int i = 0; i < MAPSIZE*MAPSIZE; i++){
		model->flux[i] = totalFlux();
		model->sedimentAmount[i] = 0.0f;
		model->velocityVector[i] = Vec2D();
		model->waterHeight[i] = 0.0f;
		avgWater[i] = 0.0f;

		model->rockMap[i] = stack<rockType>();
	}

	PerlinNoise noise(rand());
	rainMap = noise.genNoiseMap();

	for(int i = 0; i < MAPSIZE*MAPSIZE; i++){
		rainMap[i] = rainMap[i]*RAINSCALE;
	}

	std::cout << "Starting rock generation \n";
	
	//perlin rocks
	//REMEMBER TO ADJUST FOR SCALING
	for(int i = 0; i <= ROCKLAYERS; i++){
		PerlinNoise noise(rand()); 
		noise.genNoiseMap();
		rockTypes type = static_cast<rockTypes>(rand() % NUM_ROCKS);
		for(int j = 0; j < MAPSIZE*MAPSIZE; j++){
			noise.map[j] = noise.map[j]*50.0;
		}

		for(int j = 0; j < MAPSIZE*MAPSIZE; j++){
			double rockHeight = noise.map[j];
			rockType rType;
			if(!model->rockMap[j].empty()){
				if(!(model->rockMap[j].top().top + rockHeight > HEIGHTFACTOR)){
					rType.bot = model->rockMap[i].top().top;
					rType.top = rType.bot+rockHeight;
					rType.type = type;
					
					model->rockMap[j].push(rType);
				}
			} else {
				rType.bot = 0.0;
				rType.top = rockHeight;
				rType.type = type;

				model->rockMap[j].push(rType);
			}
			
		}
		delete[] noise.map;
		cout << "Layer: " << i << "\n";
	}
	
	/*
	//Straight layer rocks
	for(int i = 0; i < 6; i ++){
		for(int j = 0; j < MAPSIZE*MAPSIZE; j++){
			rockType rType;
			if(i != 0)
				rType.bot = 175+i*25;
			else
				rType.bot = 0;
			rType.top = 175+(i+1)*25;
			rType.type = static_cast<rockTypes>(i % NUM_ROCKS);

			model->rockMap[j].push(rType);
		}
	}
	*/

	/*
	//Slope Rocks
	for(int i = 0; i < MAPSIZE; i++){
		for(int j = 0; j < MAPSIZE; j++){
			rockType rType;
			rType.bot = -0.1;
			rType.top = HEIGHTFACTOR;
			if(j <= 341){
				rType.type = GRANITE;
			} else if(j >= 341 && j <= 341*2){
				rType.type = SANDSTONE;
			} else {
				rType.type = LIMESTONE;
			}

			model->rockMap[getIndex(j, i)].push(rType);
		}
	}
	*/
}

void WaterAgent::erode() {

	for(int i = 0; i < MAPSIZE*MAPSIZE; i++){
		if(model->rockMap[i].empty())
			cout << "why?";
		if(!model->rockMap[i].empty()){
			while(model->heightMap[i] < model->rockMap[i].top().bot){
				model->rockMap[i].pop();
			}
		}
		
	}

	//Step 1 - WaterIncrement
	distributeWater();
	//Step 2 - FlowSimulation
	simulateFlow();
	//Step 3 - ErosionDeposition
	erosionDeposition();
	//Step 4 - SedimentTransport
	sedimentTransport();
	//Step 5 - Evaporation
	evaporation();
}

void WaterAgent::addSprings(){
	int numsprings = 10;//rand() % 5;

	for(int i = 0; i < numsprings; i++){
		springs.push_back(rand() % MAPSIZE*MAPSIZE);
	}
}

//Add water to the height map from rain and springs.
void WaterAgent::distributeWater(){
	for(int i = 0; i <MAPSIZE*MAPSIZE; i++) {
		double minRain;
		double maxRain;

		/*if(season == SUMMER) {
			minRain = SUMMERMINRAIN + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(SUMMERMAXRAIN-SUMMERMINRAIN)));
			maxRain = minRain+RAINVARIATION;
		} else {
			minRain = WINTERMINRAIN + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(WINTERMAXRAIN-WINTERMINRAIN)));
			maxRain = minRain+RAINVARIATION;
		}

		model->waterHeight[i] += DELTATIME*(minRain+ static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(maxRain-minRain))));
		*/
		/*
		if(std::find(springs.begin(), springs.end(), i) != springs.end()){
			model->waterHeight[i] += DELTATIME*(MINSPRINGVOLUME+ static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(MAXSPRINGVOLUME - MINSPRINGVOLUME))));
		}
		*/
		//model->waterHeight[i] += 0.25 + rainMap[i]*RAINSCALE;
		//model->waterHeight[i] += 0.25;
		if(model->waterHeight[i] <= 0.0)
			model->waterHeight[i] += 0.000000000000000001;

		avgWater[i] = model->waterHeight[i];
	}

	//model->waterHeight[getIndex(513,513)] += 10;
	model->waterHeight[getIndex(170, 900)] += 100;
	model->waterHeight[getIndex(513, 900)] += 100;
	model->waterHeight[getIndex(854, 900)] += 100;
}

//Boundary cells need to have 0 flow out of the system.
void WaterAgent::simulateFlow(){
	for(int i = 0; i <MAPSIZE*MAPSIZE;i++){	
		Vec2D pos = getCoords(i);

		//Modelled as if 0,0 is at the bottom left. Up is y+1, down is y-1
		double flowL = 0;
		double flowR = 0;
		double flowU = 0;
		double flowD = 0;

		//Calculate outflow
		if(pos.x - 1 >= 0){
			flowL = calculateFlow(i, getIndex(pos.x-1, pos.y), model->flux[i].leftFlux);
		}

		if(pos.x + 1 < MAPSIZE){
			flowR = calculateFlow(i, getIndex(pos.x+1, pos.y), model->flux[i].rightFlux);
		}

		if(pos.y - 1 >= 0){
			flowD = calculateFlow(i, getIndex(pos.x, pos.y-1), model->flux[i].downFlux);
		}

		if(pos.y + 1 < MAPSIZE){
			flowU = calculateFlow(i, getIndex(pos.x, pos.y+1), model->flux[i].upFlux);
		}

		//Scale as neccesary
		double k = (model->waterHeight[i]*GRIDAREA)/((flowL+flowR+flowU+flowD)*DELTATIME);
		if(k > 1) {
			k = 1.0;
		} else {
		}

		model->flux[i].leftFlux = flowL*k;
		model->flux[i].rightFlux = flowR*k;
		model->flux[i].upFlux = flowU*k;
		model->flux[i].downFlux = flowD*k;
	}

	//Update Water Heights
	for(int i = 0; i < MAPSIZE*MAPSIZE; i++){
		Vec2D pos = getCoords(i);

		double inFlowL = 0;
		double inFlowR = 0;
		double inFlowU = 0;
		double inFlowD = 0;

		double tmpLF = 0.0;
		double tmpRF = 0.0;
		double tmpDF = 0.0;
		double tmpUF = 0.0;

		double avgWaterX = 0.0;
		double avgWaterY = 0.0;
		
		if(pos.x - 1 >= 0){
			inFlowL = model->flux[getIndex(pos.x-1, pos.y)].rightFlux;
		}

		if(pos.x + 1 < MAPSIZE){
			inFlowR = model->flux[getIndex(pos.x+1, pos.y)].leftFlux;
		}

		if(pos.y - 1 >= 0){
			inFlowD = model->flux[getIndex(pos.x, pos.y-1)].upFlux;
		}

		if(pos.y + 1 < MAPSIZE){
			inFlowU = model->flux[getIndex(pos.x, pos.y+1)].downFlux;
		}

		//Equation 6
		double deltaV = DELTATIME * (inFlowL + inFlowR + inFlowD + inFlowU
			- model->flux[i].leftFlux - model->flux[i].rightFlux
			- model->flux[i].downFlux - model->flux[i].upFlux);

		//Equation 7
		model->waterHeight[i] += deltaV/GRIDAREA;

		if (model->waterHeight[i] < 0)
			model->waterHeight[i] = 0;

		avgWater[i] = (avgWater[i]+model->waterHeight[i])/2;

		//Equation 8
		if(pos.x - 1 >= 0)
			tmpRF = model->flux[getIndex(pos.x - 1, pos.y)].rightFlux;
		if(pos.x + 1 < MAPSIZE)
			tmpLF = model->flux[getIndex(pos.x + 1, pos.y)].leftFlux;
		if(pos.y - 1 >= 0)
			tmpUF = model->flux[getIndex(pos.x, pos.y - 1)].upFlux;
		if(pos.y + 1 < MAPSIZE)
			tmpDF = model->flux[getIndex(pos.x, pos.y + 1)].downFlux;

		avgWaterX =
			(tmpRF -
			model->flux[i].leftFlux + 
			model->flux[i].rightFlux - 
			tmpLF)/2.0;
		//Similar as Equation 8 just for Y
		avgWaterY = 
			(tmpUF - 
			model->flux[i].downFlux +
			model->flux[i].upFlux -
			tmpDF)/2.0;
		
		model->velocityVector[i].x = avgWaterX / (GRIDSPACING * avgWater[i]);
		model->velocityVector[i].y = avgWaterY / (GRIDSPACING * avgWater[i]);
	}

}

double WaterAgent::calculateFlow(int curIndex, int newIndex, double flux){
	double flow = 0;
	double heightDif;
	heightDif =
		model->heightMap[curIndex] + 
		model->waterHeight[curIndex] - 
		model->heightMap[newIndex] - 
		model->waterHeight[newIndex];
	flow = flux+DELTATIME * PIPEAREA*((GRAVITY*heightDif)/GRIDSPACING); //Not really sure what the cross-section of the pipe is.
	if(flow < 0){
		flow = 0.0;
	}

	return flow;
}

/*
*	C(x,y) = Kc * sin(alpha(x,y)) * |velocity(x,y)|
*
*/
double WaterAgent::calculateTransport(Vec2D velocityVector, int index) {
	//Equation 10
	return SEDIMENTCAPACITY * sin(getTiltAngle(velocityVector, index)) * getLength(velocityVector);
}

double WaterAgent::getTiltAngle(Vec2D velVector, int index){
	/*
	Vec2D startpoint = getCoords(index);
	Vec2D endpoint = startpoint + velVector;
	double heightDif = model->heightMap[index] - interpolateHeight(endpoint.x, endpoint.y);
	return atan2(heightDif, velVector.length());
	*/

	
	double leftHeight = 0.0;
	double rightHeight = 0.0;
	double upHeight = 0.0;
	double downHeight = 0.0;

	Vec2D coords = getCoords(index);
	int x = coords.x;
	int y = coords.y;

	if(x-1 > 0)
		leftHeight = model->heightMap[getIndex(x-1,y)];

	if(x+1 < MAPSIZE)
		rightHeight = model->heightMap[getIndex(x+1,y)];

	if(y+1 < MAPSIZE)
		upHeight = model->heightMap[getIndex(x,y+1)];

	if(y-1 > 0)
		downHeight = model->heightMap[getIndex(x,y-1)];

	double minHeight = min(leftHeight, min(rightHeight, min(upHeight, downHeight)));

	return atan2(model->heightMap[index] - minHeight, GRIDSPACING);
	
}

/**
 *	This function updates the sediment amount s.
 *	It also sets the terrain height for the next time step b_t + Δt.
*/
void WaterAgent::erosionDeposition() {
	double capacity = 0.0; //C
	double rockDissolve = DISSOLVING;
	double rockDeposit = DEPOSITION;

	for(int i = 0; i < MAPSIZE*MAPSIZE; i++) {
		capacity = calculateTransport(model->velocityVector[i], i);

		rockTypes topType = GRANITE;

		if(!model->rockMap[i].empty())
			topType = model->rockMap[i].top().type;

		switch (topType)
		{
		case GRANITE:
			rockDissolve = GRANITEDISSOLVE;
			rockDeposit = GRANITEDEPOSIT;
			break;
		case SANDSTONE:
			rockDissolve = SANDSTONEDISSOLVE;
			rockDeposit = SANDSTONEDEPOSIT;
			break;
		case LIMESTONE:
			rockDissolve = LIMESTONEDISSOLVE;
			rockDeposit = LIMESTONEDEPOSIT;
			break;
		default:
			rockDissolve = GRANITEDISSOLVE;
			rockDeposit = GRANITEDEPOSIT;
			break;
		}

		if (capacity < 0.0)
			capacity = 0.0;

		double totalDissolve = min(rockDissolve * (capacity - model->sedimentAmount[i]), 
			model->waterHeight[i]- model->sedimentAmount[i]);

		if (capacity > model->sedimentAmount[i]) {
			model->heightMap[i] -= totalDissolve;
			model->sedimentAmount[i] += totalDissolve;
		} else {
			model->heightMap[i] += rockDeposit * (model->sedimentAmount[i] - capacity);
			model->sedimentAmount[i] -= rockDeposit * (model->sedimentAmount[i] - capacity);
		}

		if (model->heightMap[i] < 0)
			model->heightMap[i] = 0;
	}
}

/**
 *	This function sets the sediment amount for the next time step s_t+Δt.
 */
void WaterAgent::sedimentTransport() {
	Vec2D backStep, pos;

	for(int i = 0; i < MAPSIZE*MAPSIZE; i++) {
		pos = getCoords(i);
		backStep = Vec2D(pos.x - model->velocityVector[i].x*DELTATIME, pos.y - model->velocityVector[i].y*DELTATIME);

		model->sedimentAmount[i] = interpolateSediment(backStep.x, backStep.y);
	}
}

/**
 *	This function sets the water height for the next time step d_t+Δt.
 */
void WaterAgent::evaporation() {
	//Equation 15
	for(int i = 0; i < MAPSIZE*MAPSIZE; i++) {
		model->waterHeight[i] = model->waterHeight[i] * (1 - EVAPORATION * DELTATIME);
		
		if (model->waterHeight[i] < 0)
			model->waterHeight[i] = 0;
	}
}

void WaterAgent::thermalErosion(){
	for(int i = 0; i < MAPSIZE*MAPSIZE; i++){
		double Rt;
		double talus;
		Vec2D lowest;
		vector<Vec2D> lowerNeighbors;
		double height = model->heightMap[i];


		switch (model->rockMap[i].top().type)
		{
		case GRANITE:
			Rt = 0.6;
			talus = 45;
		case SANDSTONE:
			Rt = 0.8;
			talus = 35;
		case LIMESTONE:
			Rt = 1.0;
			talus = 30;
		default:
			Rt = 1.0;
			talus = 45;
			break;
		}

		double totalHeight = processNeighbors(lowest, lowerNeighbors, talus, i);

		if(totalHeight <= 0){
			continue;
		}
		

		double soilAmount = DELTATIME*Rt*((height - model->heightMap[getIndex(lowest.x, lowest.y)])*EROSIONAMOUNT);

			for(vector<Vec2D>::iterator it = lowerNeighbors.begin(); it != lowerNeighbors.end(); ++it){
				int index = getIndex((*it).x, (*it).y);
				model->heightMap[index] += soilAmount * (model->heightMap[index] / totalHeight);
			}
			model->heightMap[i] -= soilAmount;
	}
}

double WaterAgent::processNeighbors(Vec2D& lowest, vector<Vec2D>& lowerNeighbors, double talus, int index){
	double totalHeight = 0;
	Vec2D pos = getCoords(index);
	double height = model->heightMap[index];
	double lowVal = height;
	vector<Vec2D> neighbors;

	checkNeighbor(lowest, lowerNeighbors, talus, lowVal, height, totalHeight, Vec2D(pos.x-1, pos.y));
	checkNeighbor(lowest, lowerNeighbors, talus, lowVal, height, totalHeight, Vec2D(pos.x+1, pos.y));
	checkNeighbor(lowest, lowerNeighbors, talus, lowVal, height, totalHeight, Vec2D(pos.x, pos.y-1));
	checkNeighbor(lowest, lowerNeighbors, talus, lowVal, height, totalHeight, Vec2D(pos.x, pos.y+1));

	return totalHeight;
}

void WaterAgent::checkNeighbor(Vec2D& lowest, vector<Vec2D>& lowerNeighbors, double talus, double& lowVal, double height, double &totalHeight, Vec2D pos){

	if(pos.x  < 0 || pos.y < 0 || pos.x >= MAPSIZE || pos.y >= MAPSIZE)
		return;

	double curHeight = model->heightMap[getIndex(pos.x, pos.y)];
		if(curHeight < height){
			double talusAngle = atan2(height-curHeight, GRIDSPACING) * 180 / M_PI; 
			if(talusAngle > talus){
				lowerNeighbors.push_back(pos);
				totalHeight += curHeight;
				if(curHeight < lowVal){
					lowest.x = pos.x;
					lowest.y = pos.y;
					lowVal = curHeight;
				}
			}
		}
}

double WaterAgent::interpolateSediment(double x, double y){
	double c1 = 0.0;
	double c2 = 0.0;
	double c3 = 0.0;
	double c4 = 0.0;

	int floorX = (int)floor(x);
	int floorY = (int)floor(y);
	int ceilX = (int)ceil(x);
	int ceilY = (int)ceil(y);

	bool outOfBounds = floorX < 0 || floorY < 0 || ceilX > MAPSIZE-1 || ceilY > MAPSIZE-1;

	if(!outOfBounds)
		double c1 = model->sedimentAmount[getIndex(floorX, floorY)];
	if(!outOfBounds)
		double c2 = model->sedimentAmount[getIndex(ceilX, floorY)];
	if(!outOfBounds)
		double c3 = model->sedimentAmount[getIndex(floorX, ceilY)];
	if(!outOfBounds)
		double c4 = model->sedimentAmount[getIndex(ceilX, ceilY)];

	double R2 = (ceilX - x)*c1 + (x - floorX)*c2;
	double R1 = (ceilX - x)*c3 + (x - floorX)*c4;
	return (ceilY - y)*R1 + (y - floorY)*R2;
}

double WaterAgent::interpolateHeight(double x, double y){
	double c1 = 0.0;
	double c2 = 0.0;
	double c3 = 0.0;
	double c4 = 0.0;

	int floorX = (int)floor(x);
	int floorY = (int)floor(y);
	int ceilX = (int)ceil(x);
	int ceilY = (int)ceil(y);

	bool outOfBounds = floorX < 0 || floorY < 0 || ceilX > MAPSIZE-1 || ceilY > MAPSIZE-1;

	if(!outOfBounds)
		double c1 = model->heightMap[getIndex(floorX, floorY)];
	if(!outOfBounds)
		double c2 = model->heightMap[getIndex(ceilX, floorY)];
	if(!outOfBounds)
		double c3 = model->heightMap[getIndex(floorX, ceilY)];
	if(!outOfBounds)
		double c4 = model->heightMap[getIndex(ceilX, ceilY)];

	double R2 = (ceilX - x)*c1 + (x - floorX)*c2;
	double R1 = (ceilX - x)*c3 + (x - floorX)*c4;
	return (ceilY - y)*R1 + (y - floorY)*R2;
}