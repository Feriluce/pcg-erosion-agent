#ifndef TERRAINMODEL_H
#define TERRAINMODEL_H

#include "vec2D.h"
#include <stack>

enum Season{
	SUMMER, WINTER
};

enum rockTypes{
	GRANITE,
	SANDSTONE,
	LIMESTONE,
	NUM_ROCKS
};

struct totalFlux{
	double leftFlux;
	double rightFlux;
	double upFlux;
	double downFlux;

	totalFlux():leftFlux(0), rightFlux(0), upFlux(0), downFlux(0) { }
};

struct rockType{
	rockTypes type;
	double top;
	double bot;

	rockType():type(GRANITE), top(0), bot(0) { }
};

struct terrainModel{
	double* heightMap;
	double* waterHeight;
	double* sedimentAmount;
	totalFlux* flux;
	Vec2D* velocityVector;
    std::stack<rockType>* rockMap;
};


//Could be problematic. Not entirely sure if this is the correct order
int getIndex(int x, int y);
Vec2D getCoords(int x);

double getAngle(Vec2D velocityVector);
double getLength(Vec2D velocityVector);
double* createSlope();

#endif